var fs = require("fs");
var spellData = JSON.parse(fs.readFileSync("cleric_cantrips.json", 'utf8'));


function splitDescription(spellText) {
  var strippedHTML = spellText.replace(/<a[^>]+>/g, '');
  strippedHTML = strippedHTML.replace(/<\/a>/g, '');
  if (strippedHTML.length > 500) {
    var idx = strippedHTML.indexOf(' ', 499);
    return [strippedHTML.substring(0, idx).trim(), strippedHTML.substring(idx, 1500).trim()]
  } else {
    return [strippedHTML, ''];
  }
}

function getFrontCardHTML(spell) {

  var castingTime = spell.castingTime;
  if (spell.ritual) {
    castingTime += ' (R)';
  }
  var duration = spell.duration;
  if (spell.concentration) {
    duration += ' (C)';
  }

  var spellText = splitDescription(spell.spellText)[0];

  return `
<div class="Card">
  <h3 class="Card-title">${spell.spellName}</h3>
  <span class="Card-subtitle">${spell.school} ${spell.level}</span>
  <div class="Card-details">
    <div class="Card-detail Card-castingTime">Casting time <span>${castingTime}</span></div>
    <div class="Card-detail Card-range">Range <span>${spell.range}</span></div>
    <div class="Card-detail Card-distance">Duration <span>${duration}</span></div>
    <div class="Card-detail Card-components">Components <span>${spell.components}</span></div>
  </div>
  <div class="Card-description">
    <div class="Card-text">${spellText}</div>
    <div class="Card-text Card-text-right">${spell.spellSource}</div>
  </div>
</div>
`;
}

function getBackCardHTML(spell) {
  var spellText = splitDescription(spell.spellText)[1];
  if (spellText.trim() === '') {
    return '';
  } else {
    return `
  <div class="Card" >
      <h3 class="Card-title" >${spell.spellName}</h3>
      <div class="Card-description">
          <div class="Card-text">${spellText}</div>
      </div>
  </div>
  `;
  }
}

var spellCards = '';
Object.keys(spellData).forEach(function(spellId) {
  var spell = spellData[spellId];

  spellCards += getFrontCardHTML(spell);
  spellCards += getBackCardHTML(spell);
});


var spellBook = fs.readFileSync("spellbook.html", 'utf8');
spellBook = spellBook.replace('${spellBook}', spellCards);

console.log(spellBook);
