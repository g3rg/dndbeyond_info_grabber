var fetchCount=0;

function fetchSpell(spellId, spellName, spells) {
  var moreInfoUrl = 'https://www.dndbeyond.com/spells/' + spellId + '/more-info';

  $.get(moreInfoUrl, function( data ) {

    var statblock = $($.parseHTML(data)[2].childNodes[1].childNodes[1]);

    var level = statblock.find('.ddb-statblock-item-value')[0].childNodes[0].nodeValue.trim(),
        ritual = (statblock.find('.ddb-statblock-item-value')[1].innerHTML.indexOf('ritual') > -1),
        castingTime = statblock.find('.ddb-statblock-item-value')[1].childNodes[0].nodeValue.trim(),
        concentration = (statblock.find('.ddb-statblock-item-value')[4].innerHTML.indexOf('concentration') > -1),
        range = statblock.find('.ddb-statblock-item-value')[2].childNodes[0].nodeValue.trim() +

            (statblock.find('.ddb-statblock-item-value')[2].childNodes.length == 1 ? ''
                : ' ' + statblock.find('.ddb-statblock-item-value')[2].childNodes[1].innerHTML.replace('<i class="i-aoe-', '').replace('"></i>', '')),
        components = $(statblock.find('.ddb-statblock-item-value')[3].innerHTML.trim())[0].innerHTML,
        duration = statblock.find('.ddb-statblock-item-value')[4].innerHTML.replace('<i class="i-concentration">Concentration</i>', '').trim(),
        school = statblock.find('.ddb-statblock-item-value')[5].childNodes[0].nodeValue.trim(),


        attackSave = $(statblock.find('.ddb-statblock-item-value')[6].innerHTML.trim())[0].innerHTML === 'None' ?
            $(statblock.find('.ddb-statblock-item-value')[6].innerHTML.trim())[0].innerHTML
            : $(statblock.find('.ddb-statblock-item-value')[6].innerHTML.trim())[2].innerHTML.trim();

    damageEffect = statblock.find('.ddb-statblock-item-value')[7].childNodes[0].nodeValue.trim() !== '' ?
        statblock.find('.ddb-statblock-item-value')[7].childNodes[0].nodeValue.trim()
        : statblock.find('.ddb-statblock-item-value')[7].childNodes[2].nodeValue.trim().trim(),

        spellClasses = $($.parseHTML(data)).find('.more-info-footer-classes'),
        spellSource = $($.parseHTML(data)).find('.more-info-footer-source')[0].innerHTML.trim();

    spellText = $($.parseHTML(data)).find('.more-info-body-description')[0].innerHTML.trim();

    let details = {
      spellId, spellName, level, ritual,
      castingTime, concentration, range,
      components, duration, school, attackSave,
      damageEffect, spellText, spellSource, spellClasses};
    spells[spellId] = details;
    fetchCount--;

  });
}

function checkAndOutput() {
  if (fetchCount > 0) {
    setTimeout("checkAndOuput()", 1000);
  } else {
    console.log(JSON.stringify(spells));
  }
}

function fetchAllPages(spells) {
  var fullList = {};

  // Get current page:
  var spellSlugs = $("div[data-slug]");
  spellSlugs.each(function(slugIdx) {
    var slug = spellSlugs[slugIdx];
    var spellId = slug.attributes['data-slug'].value;
    var spellName = $($(slug.childNodes).filter('.row.spell-name')[0].childNodes).filter('.name').text().trim();

    fullList[spellId] = spellName;
  });

  var pageList = $("a.b-pagination-item");
  var pageListUrlTemplate = 'https://www.dndbeyond.com/spells?page=';
  var maxPage = 0;
  for (var i=0;i<pageList.length;i++) {
    let pageNum = parseInt(pageList[i].href.substring(pageList[i].href.indexOf('page')+5).trim());
    if (pageNum > maxPage) maxPage = pageNum;
  }
  for (var pageNum=2;pageNum<=maxPage;pageNum++) {
    let pageUrl = pageListUrlTemplate + pageNum;
    console.log("Should fetch " + pageUrl);
  }

  console.log("Total pages found: " + maxPage);

}


function fetchCurrentPage(spells) {
  var spellSlugs = $("div[data-slug]");
  spellSlugs.each(function(slugIdx) {
    var slug = spellSlugs[slugIdx];

    var spellId = slug.attributes['data-slug'].value; // 'etherealness';
    var spellName = $($(slug.childNodes).filter('.row.spell-name')[0].childNodes).filter('.name').text().trim();
    fetchCount++;
    fetchSpell(spellId, spellName, spells);
  });
}

function fetchOneSpell(spellId, spellName, spells) {
  fetchCount++;
  fetchSpell(spellId, spellName, spells);
}

var spells = {};

//fetchOneSpell('detect-magic', 'Detect Magic', spells);
//fetchCurrentPage(spells);

fetchAllPages(spells);

setTimeout(checkAndOutput, 5000);
